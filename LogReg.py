#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd
import numpy as np
import copy
import matplotlib.pyplot as plt
from sklearn import svm
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import matthews_corrcoef, confusion_matrix, f1_score, classification_report
from sklearn.model_selection import GridSearchCV, learning_curve, ShuffleSplit
from sklearn.utils import shuffle


# In[2]:


file_path = "results/log_reg.csv"


# ### Loading Dataset

# In[3]:


file1 = pd.read_csv("./datasets/sign_mnist_train.csv")
y_train = file1["label"].values
file1 = file1.drop("label", axis=1)
X_train = file1.values


file2 = pd.read_csv("./datasets/sign_mnist_test.csv")
y_test = file2["label"].values
file2 = file2.drop("label", axis=1)
X_test = file2.values

#dataset = pd.concat([file1, file2], axis=0)
#y = dataset["label"].values
#dataset = dataset.drop("label", axis=1)
#X = dataset.values

#X, y = shuffle(X, y, random_state=0)

#X_train = X[:int(len(X)*0.8)]
#X_test = X[int(len(X)*0.8):]
#y_train = y[:int(len(y)*0.8)]
#y_test = y[int(len(y)*0.8):]


# ### Normalizing the Data

# In[4]:


mean = np.mean(X_train, axis=0)
std_d = np.std(X_train, axis=0)

X_train = (X_train-mean)/std_d
X_test = (X_test-mean)/std_d


# ### Grid Search of Best Parameters

# In[5]:


estimator = LogisticRegression()
params = {
    "penalty" : ["l2"],
    "C" : [0.0001, 0.001, 0.01, 0.1, 1],
    "solver" : ["newton-cg", "lbfgs", "liblinear", "sag", "saga"],
    "max_iter" : [10000],
}

gridsearch = GridSearchCV(estimator=estimator, param_grid=params, scoring="f1_macro", n_jobs=10, cv=5, verbose=2)
gridsearch.fit(X_train, y_train)


# ### Obtain Best Classifier

# In[6]:


logreg = gridsearch.best_estimator_
print("Score Obtained by the best classifier: ", gridsearch.best_score_)
print("Best Classifier Parameters: ", gridsearch.best_params_)
f = open("results/best_classifier_log_reg.txt", "w")
f.write("Best Score: " + str(gridsearch.best_score_) + "\n")
f.write("Best Params: " + str(gridsearch.best_params_) + "\n")
f.close()


# ### Store all the results

# In[7]:


results = pd.DataFrame(gridsearch.cv_results_)
results.to_csv(file_path)


# ### Plot 10 best classifiers

# In[8]:



N_BEST_RESULTS = 20
clf_results = copy.deepcopy(gridsearch.cv_results_)
results = list(zip(clf_results['params'], clf_results['mean_test_score']))
# get the best N_BEST_RESULTS combination of hyperparameters
results = sorted(results, key=lambda el: el[1])
results = results if len(results) < N_BEST_RESULTS else results[-N_BEST_RESULTS:]

x = np.array(range(1, len(results)+1))
y = [el[1] for el in results]
my_xticks = [el[0] for el in results]
for dic in my_xticks:
    del dic["max_iter"]

# plot
plt.style.use('default')
fig = plt.figure(figsize=(30, 5), dpi=80)
plt.xticks(x, my_xticks, rotation=90)
plt.plot(x, y)
plt.ylim(0, 1)
plt.xlabel('Training Parameters')
plt.ylabel('F1 Score')
plt.title("Logistic Regression Hyperparameters")
plt.show()
# save figure
fig.savefig('figures/log_Reg_hyperparam.png', dpi=fig.dpi, bbox_inches='tight')


# ### Obtain Learning Curve

# In[9]:


#logreg = LogisticRegression(**{'C': 1, 'max_iter': 10000, 'penalty': 'l2', 'solver': 'liblinear'})
#logreg.fit(X_train, y_train)
#estimator = LogisticRegression(**{'C': 1, 'max_iter': 10000, 'penalty': 'l2', 'solver': 'liblinear'})

estimator = LogisticRegression(**gridsearch.best_params_)

cv = ShuffleSplit(n_splits=5, test_size=0.16, random_state=0)

fig = plt.figure()
plt.title("Learning Curve of Logistic Regression")

plt.ylim(*(0.7, 1.01))

plt.xlabel("Training examples")
plt.ylabel("Score")
train_sizes, train_scores, test_scores = learning_curve(
    estimator, X_train, y_train, cv=cv, n_jobs=-1, train_sizes=np.linspace(.1, 1.0, 5))

train_scores_mean = np.mean(train_scores, axis=1)
train_scores_std = np.std(train_scores, axis=1)
test_scores_mean = np.mean(test_scores, axis=1)
test_scores_std = np.std(test_scores, axis=1)

plt.grid()

plt.fill_between(train_sizes, train_scores_mean - train_scores_std,
                 train_scores_mean + train_scores_std, alpha=0.1,
                 color="r")
plt.fill_between(train_sizes, test_scores_mean - test_scores_std,
                 test_scores_mean + test_scores_std, alpha=0.1, color="g")
plt.plot(train_sizes, train_scores_mean, 'o-', color="r", 
         label="Training score")
plt.plot(train_sizes, test_scores_mean, 'o-', color="g", 
         label="Cross-validation score")

plt.legend(loc="best")
plt.show()
fig.savefig('figures/learning_curve_log_reg.png', dpi=fig.dpi, bbox_inches='tight')


# ### Test classifier with unseen data

# In[10]:


y_pred = logreg.predict(X_test)


# ### Confusion Matrix plot

# In[11]:


cm = confusion_matrix(y_test, y_pred)
# plot confusion matrix
fig = plt.figure()
ax = fig.add_subplot(111)
cax = ax.matshow(cm)
plt.title('Confusion matrix')
fig.colorbar(cax)
labels = [i+1 for i in range(0,len(cm))]
#ax.set_xticklabels(labels)
#ax.set_yticklabels(labels)
plt.xlabel('Predicted Class')
plt.ylabel('Real Class')
plt.show()
fig.savefig('figures/confusion_matrix_log_reg.png', dpi=fig.dpi, bbox_inches='tight')


# ### Classification Report

# In[12]:


report = classification_report(y_test , y_pred, labels=np.unique(y_test),  output_dict=True)
df = pd.DataFrame(report).transpose()
df.to_csv("results/classification_report_log_reg.csv")
print(df)


# ### MCC and F1-Score

# In[13]:


print( "MCC: ", matthews_corrcoef(y_test, y_pred))
print( "F1-Score: ", f1_score(y_test, y_pred, average='micro'))
f = open("results/best_classifier_log_reg.txt", "a")
f.write("MCC: " + str(matthews_corrcoef(y_test, y_pred)) + "\n")
f.write("F1-Score: " + str(f1_score(y_test, y_pred, average='micro')) + "\n")
f.close()


# In[ ]:




