#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd
import matplotlib.pyplot as plt
import datetime
import copy
import csv
import os
import numpy as np
from sklearn import svm
from sklearn.model_selection import GridSearchCV,  learning_curve, ShuffleSplit
from sklearn.utils import shuffle
from scipy.sparse import coo_matrix
from sklearn.metrics import confusion_matrix, classification_report, matthews_corrcoef, f1_score, plot_confusion_matrix
from PIL import Image
import random
import shutil


# ## Auxiliary Functions

# In[2]:


def results_to_file(classifier, file_path):
    results = pd.DataFrame(classifier.cv_results_)
    current_time = datetime.datetime.now()
    results['Timestamp'] = [current_time] * results.shape[0]
    results.to_csv(file_path)
    

def plot_hyperparameter_classifier_variation(clf, title):
    N_BEST_RESULTS = 20
    clf_results = copy.deepcopy(clf.cv_results_)
    results = list(zip(clf_results['params'], clf_results['mean_test_score']))
    # get the best N_BEST_RESULTS combination of hyperparameters
    results = sorted(results, key=lambda el: el[1])
    results = results if len(results) < N_BEST_RESULTS else results[-N_BEST_RESULTS:]
    
    x = np.array(range(1, len(results)+1))
    y = [el[1] for el in results]
    my_xticks = [el[0] for el in results]
    
    # reduce label size
    for dic in my_xticks:
        dic["dec_func"] = dic["decision_function_shape"]
        del dic["decision_function_shape"]
        del dic["kernel"]
    
    # plot
    plt.style.use('default')
    fig = plt.figure(figsize=(30, 5), dpi=80)
    plt.xticks(x, my_xticks, rotation=90)
    plt.plot(x, y)
    plt.ylim(0, 1)
    plt.xlabel('Training Parameters')
    plt.ylabel('F1 Score')
    plt.title(title)
    plt.show()
    # save figure
    fig.savefig(f'figures/SVM - {title} - Hyperparameters.png', dpi=fig.dpi, bbox_inches='tight')
    

BEST_CLASSIFIER = None
def update_best_classifier(clf):
    global BEST_CLASSIFIER
    if BEST_CLASSIFIER is None or clf.best_score_ > BEST_CLASSIFIER.best_score_:
        BEST_CLASSIFIER = clf


# ## Load  and preview the data 
# 
# The file *datasets/sign_mnist_train.csv* contains the training dataset for this problem. The 1st column contains the label (y)  whereas the other columns contain the features/pixels of the images. 

# In[3]:


data_train = pd.read_csv('datasets/sign_mnist_train.csv')
data_test = pd.read_csv('datasets/sign_mnist_test.csv')

# Train features
X_train= data_train.values[:,1:]
# Train labels
y_train = data_train.values[:,0]
y_train = y_train.reshape(y_train.shape[0],1)

# Test features
X_test= data_test.values[:,1:]
# Test labels
y_test = data_test.values[:,0]
y_test = y_test.reshape(y_test.shape[0],1)


# In[4]:


data_train.head()


# In[5]:


data_train.describe()


# ## Normalize the data

# In[6]:


mean = np.mean(X_train, axis=0)
std_d = np.std(X_train, axis=0)

X_train_original = copy.deepcopy(X_train)
X_test_original = copy.deepcopy(X_test)

X_train = (X_train-mean)/std_d
X_test = (X_test-mean)/std_d


# # Label Representation
# 
# Check if the dataset is balanced.

# In[7]:


# fig configuration
plt.style.use('default')
fig = plt.figure(figsize=(12, 4), dpi=80)
ax = fig.add_axes([0,0,1,1])

# data
unique, counts = np.unique(y_train, return_counts=True)
ax.bar(unique,counts)

# labels + title 
plt.ylabel('Number of Training Examples')
plt.xlabel('Class')
plt.title('Class Distribuition')
plt.show()

# save figure
fig.savefig('figures/class_distribuition_svm.png', dpi=fig.dpi, bbox_inches='tight')


# # Training Subset

# In[8]:


X_train_shuffled, y_train_shuffled = shuffle(X_train, y_train, random_state=0)
X_train_subset = X_train_shuffled
y_train_subset = y_train_shuffled


# # SVMs Training 

# ## Linear Kernel

# In[9]:


parameters_linear = {
    'kernel': ['linear'], 
    'C': [0.0001, 0.001, 0.01, 0.1, 1],
    'decision_function_shape': ['ovr', 'ovo']
}
svc_linear = svm.SVC()
clf_linear = GridSearchCV(svc_linear, parameters_linear, n_jobs=10, scoring='f1_micro', verbose=2, refit=False)
clf_linear.fit(X_train_subset, np.ravel(y_train_subset))

print("clf_linear best estimator:", clf_linear.best_params_)
print("clf_linear best score:", clf_linear.best_score_)
update_best_classifier(clf_linear)
results_to_file(clf_linear, "results/clf_linear.csv")
plot_hyperparameter_classifier_variation(clf_linear, "Linear Kernel")


# ## Polynomial Kernel

# In[10]:


parameters_polynomial = {
    'kernel': ['poly'], 
    'C': [0.0001, 0.001, 0.01, 0.1, 1], 
    'degree': [2,3,4],
    'coef0': [0.0001, 0.001, 0.01, 0.1, 1],
    'decision_function_shape': ['ovr', 'ovo']
}
svc_polynomial = svm.SVC()
clf_polynomial = GridSearchCV(svc_polynomial, parameters_polynomial, n_jobs=10, scoring='f1_micro', verbose=2, refit=False)
clf_polynomial.fit(X_train_subset, np.ravel(y_train_subset))
    
print("clf_polynomial best estimator:", clf_polynomial.best_params_)
print("clf_polynomial best score:", clf_polynomial.best_score_)
update_best_classifier(clf_polynomial)
results_to_file(clf_polynomial, "results/clf_polynomial.csv")
plot_hyperparameter_classifier_variation(clf_polynomial, "Polynomial Kernel")


# ## RBF Kernel

# In[11]:


parameters_rbf = {
    'kernel': ['rbf'], 
    'C': [0.0001, 0.001, 0.01, 0.1, 1], 
    'coef0': [0.0001, 0.001, 0.01, 0.1, 1],
    'decision_function_shape': ['ovr', 'ovo']
}

svc_rbf = svm.SVC()
clf_rbf = GridSearchCV(svc_rbf, parameters_rbf, n_jobs=10, scoring='f1_micro', verbose=2, refit=False)
clf_rbf.fit(X_train_subset, np.ravel(y_train_subset))
    
print("clf_rbf best estimator:", clf_rbf.best_params_)
print("clf_rbf best score:", clf_rbf.best_score_)
update_best_classifier(clf_rbf)
results_to_file(clf_rbf, "results/clf_rbf.csv")
plot_hyperparameter_classifier_variation(clf_rbf, "RBF Kernel")


# ## Sigmoid Kernel

# In[12]:


parameters_sigmoid = {
    'kernel': ['sigmoid'], 
    'C': [0.0001, 0.001, 0.01, 0.1, 1], 
    'coef0': [0.0001, 0.001, 0.01, 0.1, 1],
    'decision_function_shape': ['ovr', 'ovo']
}

svc_sigmoid = svm.SVC()
clf_sigmoid = GridSearchCV(svc_sigmoid, parameters_sigmoid, n_jobs=10, scoring='f1_micro', verbose=2, refit=False)
clf_sigmoid.fit(X_train_subset, np.ravel(y_train_subset))
    
print("clf_sigmoid best estimator:", clf_sigmoid.best_params_)
print("clf_sigmoid best score:", clf_sigmoid.best_score_)
update_best_classifier(clf_sigmoid)
results_to_file(clf_sigmoid, "results/clf_sigmoid.csv")
plot_hyperparameter_classifier_variation(clf_sigmoid, "Sigmoid Kernel")


# ## Results for the best classifier

# In[13]:


print("Best Classifier:")
print(BEST_CLASSIFIER.best_params_)

# save the best classifier to file
f = open("results/best_classifier_svm.txt", "w")
f.write(str(BEST_CLASSIFIER.best_params_)+"\n")
f.close()


# ## Learning Curve

# In[14]:


estimator = svm.SVC(**BEST_CLASSIFIER.best_params_)

cv = ShuffleSplit(n_splits=1, test_size=0.16, random_state=0)

fig = plt.figure()
plt.title("Learning Curve of the best SVM")

plt.ylim(*(0.7, 1.01))

plt.xlabel("Training examples")
plt.ylabel("Score")
train_sizes, train_scores, test_scores = learning_curve(
    estimator, X_train_subset, y_train_subset, cv=cv, n_jobs=-1, train_sizes=np.linspace(.1, 1.0, 50))

train_scores_mean = np.mean(train_scores, axis=1)
train_scores_std = np.std(train_scores, axis=1)
test_scores_mean = np.mean(test_scores, axis=1)
test_scores_std = np.std(test_scores, axis=1)

plt.grid()

plt.fill_between(train_sizes, train_scores_mean - train_scores_std,
                 train_scores_mean + train_scores_std, alpha=0.1,
                 color="r")
plt.fill_between(train_sizes, test_scores_mean - test_scores_std,
                 test_scores_mean + test_scores_std, alpha=0.1, color="g")
plt.plot(train_sizes, train_scores_mean, 'o-', color="r", 
         label="Training score")
plt.plot(train_sizes, test_scores_mean, 'o-', color="g", 
         label="Cross-validation score")

plt.legend(loc="best")
plt.show()
fig.savefig('figures/learning_curve_svm.png', dpi=fig.dpi, bbox_inches='tight')


# ### Test classifier with unseen data

# In[15]:


# predict classes
clf = svm.SVC(**BEST_CLASSIFIER.best_params_)
clf.fit(X_train_subset, y_train_subset)
y_pred = clf.predict(X_test)
y_pred = y_pred.reshape(y_pred.shape[0],1)


# ### Classification Report

# In[16]:


report = classification_report(y_test , y_pred, labels=np.unique(y_test),  output_dict=True)

# save to file
df = pd.DataFrame(report).transpose()
df.to_csv("results/classification_report_svm.csv")

print(df)


# ### Confusion Matrix

# In[17]:


cm = confusion_matrix(y_true=y_test,y_pred=y_pred)

# plot confusion matrix
fig = plt.figure()
ax = fig.add_subplot(111)
cax = ax.matshow(cm)
plt.title('Confusion matrix')
fig.colorbar(cax)
labels = [i+1 for i in range(0,len(cm))]
#ax.set_xticklabels(labels)
#ax.set_yticklabels(labels)
plt.xlabel('Predicted Class')
plt.ylabel('Real Class')
plt.show()
fig.savefig('figures/confusion_matrix.png', dpi=fig.dpi, bbox_inches='tight')


# ### MCC and F1-Score

# In[18]:


print( "MCC: ", matthews_corrcoef(y_test, y_pred))
print( "F1-Score: ", f1_score(y_test, y_pred, average='micro'))
f = open("results/best_classifier_svm.txt", "a")
f.write("MCC: " + str(matthews_corrcoef(y_test, y_pred)) + "\n")
f.write("F1-Score: " + str(f1_score(y_test, y_pred, average='micro')) + "\n")
f.close()


# ## Save some of the images that were wrongly classified

# In[19]:


# clean previous images
shutil.rmtree('wrong_classification_svm/')
os.mkdir('wrong_classification_svm')

wrong = (y_pred!=y_test)
X_test_wrong_classification = X_test_original[wrong[:,0]]

n = 100
c = 0
for img in random.sample(list(X_test_wrong_classification), n):
    array = np.array(img, dtype=np.uint8)
    array = array.reshape(28, 28)
    # Use PIL to create an image from the new array of pixels
    new_image = Image.fromarray(array, 'L')
    x = new_image.save(f'wrong_classification_svm/img{c}.png')
    c += 1


# In[ ]:





# In[ ]:




